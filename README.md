# SpookySOC - CURRENTLY IN BETA

SpookySOC is designed to allow security practitioners to speed up parts of their workflow, primarily OSINT and reputation analysis.

Currently, users may lookup information for **IPs** and **domains**. In the near-future, hash support will be provided.
Users will be able to have this information written to a flat-text file on their system.

**By using SpookySOC, you agree to the terms and conditions of the LICENSE, and the terms/conditions/licenses of the services providing the API key.**

**Please see [the changelog](CHANGELOG.md)** for features that are implemented and those that are planned.

## Getting Started

You can find information about getting started on the Wiki, [here](https://gitlab.com/jksn/spookySOC/-/wikis/home).

## Contributing

SpookySOC has been in development for several years in varying forms. As this is its first open-source release, contributions would be very much appreciated!
However, SpookySOC is being used as a capstone project (or "Student Innovation Project") for my university. As such, I cannot accept any code contributions **at the current time**.

Please open an issue or create a pull request, and once I graduate (by August 2020), I'll work on accepting contributions. Thank you in advance, and I'm sorry for
the odd situation.

**Beta testers are exempt from this** - I definitely need help using SpookySOC and finding issues that may arise! Please contribute by opening issues for bugs.
I just can't accept contributions in the form of code. Thanks for your help in either case!

## Image Credit

Logo image from [Free SVG](https://freesvg.org/ghost-mask-vector-image), specifically [OpenClipArt](https://freesvg.org/by/OpenClipart).
The image is licensed under CC0 / Public Domain.
